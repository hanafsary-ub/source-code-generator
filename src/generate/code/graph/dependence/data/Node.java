package generate.code.graph.dependence.data;

/**
 * データ依存グラフのノードを表すクラスです。<br>
 * 代入文としての情報とスクリーン上のプログラム文の位置をノード番号で紐付けします。
 * @author 花房亮
 *
 */
public class Node {

	private int nodeID;                            // ノード番号
	private LineRange range;                       // スクリーン上のプログラム文の位置
	private VariableDefinition variableDefinition; // 変数定義部
	private DefinitionContent definitionContent;   // 定義内容部

	/**
	 * ノード番号を指定してNodeを構築します。
	 * 
	 * @param nodeID ノード番号
	 */
	public Node(int nodeID){
		this.nodeID = nodeID;
	}

	/**
	 * ノード番号と自動生成した代入文を指定してノードを初期化します。
	 * @param nodeID ノード番号
	 * @param variableDefinition 変数定義部
	 * @param definitionContent 定義内容部
	 */
	public Node(int nodeID, VariableDefinition variableDefinition, DefinitionContent definitionContent){
		this.nodeID = nodeID;
		this.variableDefinition = variableDefinition;
		this.definitionContent = definitionContent;
	}
	
	/**
	 * ノードの変数定義部を設定します。
	 * 
	 * @param variableDefinition 変数定義部
	 */
	public void setVariableDefinition(VariableDefinition variableDefinition){
		this.variableDefinition = variableDefinition;
	}
	
	/**
	 * ノードの定義内容部を設定します。
	 * @param definitionContent 定義内容部
	 */
	public void setDefinitionContent(DefinitionContent definitionContent){
		this.definitionContent = definitionContent;
	}

	/**
	 * ノードのプログラム文の範囲を矩形範囲で設定します。
	 * @param range スクリーン上の生成されるコードが書いてある範囲
	 */
	public void setRange(LineRange range){
		this.range = range;
	}

	/**
	 * ノードのプログラム文の範囲を矩形範囲で返します。
	 * @return スクリーン上の生成されるコードが書いてある範囲
	 */
	public LineRange getRange(){
		return this.range;
	}

	/**
	 * ノード番号を返します。
	 * @return ノード番号
	 */
	public int getID(){
		return this.nodeID;
	}

	/**
	 * このノードの変数定義部を返します。
	 * @return 変数定義部
	 */
	public VariableDefinition getVariableDefinition(){
		return this.variableDefinition;
	}

	/**
	 * このノードの定義内容部を返します。
	 * @return 定義内容部
	 */
	public DefinitionContent getDefinitionContent(){
		return this.definitionContent;
	}
}
