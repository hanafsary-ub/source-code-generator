package generate.code.graph.dependence.data;

/**
 * ノード(プログラム文)に対応するソースコードの句形範囲を表現するクラスです。<br>
 * ２つあるどちらのコンストラクタで初期化しても四隅の座標、幅と高さ全て初期化することができます。
 * @author 花房亮
 *
 */
public class LineRange {

	private Coordinate topLeft;     // 左上
	private Coordinate topRight;    // 右上
	private Coordinate bottomLeft;  // 左下
	private Coordinate bottomRight; // 右下
	private int width;  // 幅
	private int height; // 高さ

	/**
	 * 矩形の四隅を指定して矩形範囲を初期化します。
	 * @param topLeft     左上の座標
	 * @param topRight    右上の座標
	 * @param bottomLeft  左下の座標
	 * @param bottomRight 右下の座標
	 */
	public LineRange(Coordinate topLeft, Coordinate topRight, Coordinate bottomLeft, Coordinate bottomRight){
		this.topLeft = topLeft;
		this.topRight = topRight;
		this.bottomLeft = bottomLeft;
		this.bottomRight = bottomRight;
		// ちなみに入力の視線データの座標の原点はスクリーンの左上である
		this.width = topRight.getX() - topLeft.getX();
		this.height = bottomLeft.getY() - topLeft.getY();
	}

	/**
	 * 左上の座標を原点とし、幅と高さを指定して矩形範囲を初期化します。
	 * @param topLeft 左上の座標
	 * @param width   矩形の幅
	 * @param height  矩形の高さ
	 */
	public LineRange(Coordinate topLeft, int width, int height){
		this.topLeft = topLeft;
		this.topRight = new Coordinate(topLeft.getX() + width, topLeft.getY());
		this.bottomLeft = new Coordinate(topLeft.getX(), topLeft.getY() + height);
		this.bottomRight = new Coordinate(topLeft.getX() + width, topLeft.getY() + height);
		this.width = width;
		this.height = height;
	}

	/**
	 * 対象とする視線の座標が範囲内にあるか判定します。
	 * 
	 * @param x 視線データのx座標
	 * @param y 視線データのy座標
	 * @return 範囲内ならtrue、範囲外ならfalse
	 */
	public boolean isWithin(int x, int y){
		boolean within = false;
		if((topLeft.getY() <= y && topLeft.getY() + height >= y) &&
		   (topLeft.getX() <= x && topLeft.getX() + width >= x))
		{
			within = true;
		}
		return within;
	}
	
	/**
	 * 対象とする視線の座標が範囲内にあるか判定します。
	 * 
	 * @param x 視線データのx座標
	 * @param y 視線データのy座標
	 * @return 範囲内ならtrue、範囲外ならfalse
	 */
	public boolean isWithin(long x, long y){
		return this.isWithin((int)x, (int)y);
	}

	/**
	 * 矩形範囲の左上の座標を返します。
	 * @return 左上の座標
	 */
	public Coordinate getTopLeft(){
		return this.topLeft;
	}

	/**
	 * 矩形範囲の右上の座標を返します。
	 * @return 右上の座標
	 */
	public Coordinate getTopRight(){
		return this.topRight;
	}

	/**
	 * 矩形範囲の左下の座標を返します。
	 * @return 左下の座標
	 */
	public Coordinate getBottomLeft(){
		return this.bottomLeft;
	}

	/**
	 * 矩形範囲の右下の座標を返します。
	 * @return 右下の座標
	 */
	public Coordinate getBottomRight(){
		return this.bottomRight;
	}

	/**
	 * 矩形範囲の幅を返します。
	 * @return　矩形範囲の幅
	 */
	public int getWidth(){
		return this.width;
	}

	/**
	 * 矩形範囲の高さを返します。
	 * @return 矩形範囲の高さ
	 */
	public int getHeight(){
		return this.height;
	}
}
