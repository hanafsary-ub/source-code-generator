package generate.code.graph.dependence.data;

/**
 * 代入文X = YのXの部分である変数定義部を表現するクラスです。<br>
 * このクラスは定義される変数の変数名を保持します。<br>
 * また、ゼロ除算などを防ぐためとして定義される変数の値も保持します。
 * @author 花房亮
 *
 */
//TODO:扱う変数の型を増やすならばこのクラスを抽象クラスあるいはインターフェースにする
public class VariableDefinition {
	private String name; // 変数名
	private int value;   // 変数に代入される整数値

	/**
	 * 変数名を初期化します。
	 * @param variableName 変数名
	 */
	public VariableDefinition(String variableName){
		this.name = variableName;
	}
	public VariableDefinition(String variableName, int variableValue){
		this.name = variableName;
		this.value = variableValue;
	}
	
	public void setName(String variableName){
		this.name = variableName;
	}

	/**
	 * 変数の値を設定します。
	 * @param variableValue 変数に代入される値
	 */
	public void setValue(int variableValue){
		this.value = variableValue;
	}
	
	/**
	 * 変数定義部で定義される変数の変数名を返します。
	 * @return 変数名
	 */
	public String getName(){
		return this.name;
	}

	/**
	 * 定義される変数に代入される値を返します。
	 * @return 変数に代入される値
	 */
	public int getValue(){
		return this.value;
	}
}
