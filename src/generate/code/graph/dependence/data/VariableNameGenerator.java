package generate.code.graph.dependence.data;

import java.util.HashSet;
import java.util.Set;

/**
 * 重複させずに変数名をランダムに決めたいときに使用するクラスです。<br>
 * 割り当てる予定の変数の数が1つであれば必ずaが割り当てられます。<br>
 * 割り当てる予定の変数の数が2つであればaまたはbがランダムな順番で割り当てられます。<br>
 * 割り当てる予定の変数の数が26個あればaからzまでがランダムな順番で割り当てられます。<br>
 * zの次はbaに繰り上がる。ba,bb,...,bz,ca,...,zz,baaというふうに割り当てられる変数の最大値は26進数に等しいです。<br><br>
 * 基本的な使い方としては以下の様な使い方をします。<br>
 * VariableNameGenerator g = new VariableNameGenerator(3); // 変数の数を3つに指定<br>
 * String name = g.assign(); // a,b,cのうちランダムに１つ割り当てる<br>
 * nameがbだったとすると<br>
 * name = g.assign(); // a,cのうちランダムに１つ割り当てる<br>
 * nameがaだったとすると<br>
 * name = g.assign(); // 残りのcが割り当てられる<br>
 * name = g.assign(); // nameの値は空文字列になる
 * 
 * @author 花房亮
 *
 */
public class VariableNameGenerator {

	private Set<String> assignedName = new HashSet<String>(); // 割り当て済みの変数名
	private int size; // 変数の数
	
	/**
	 * 使用する変数名の数を1つとしてVariableNameGeneraotorオブジェクトを構築します。
	 */
	public VariableNameGenerator(){
		this.size = 1;
	}

	/**
	 * 使用する変数の数を指定してVariableNameGeneraotorオブジェクトを構築します。<br>
	 * 変数の数sizeが1以上でなければ代わりに変数の数は1つとして構築します。
	 * 
	 * @param size 名前を割り当てる変数の数
	 */
	public VariableNameGenerator(int size){
		// sizeは1より大きくなければならない
		if(size > 0){
			this.size = size;
		}else{
			this.size = 1;
		}
	}
	
	/**
	 * 名前を割り当てる予定の変数の数を設定します。<br>
	 * 変数の数は1より少なければ、代わりに変数の数は１つとして設定され、falseが返されます。<br>
	 * また、変数の数を設定し直すと割り当て済みの変数名の集合は初期化されます。
	 * 
	 * @param size 名前を割り当てる変数の数
	 * @return sizeが1より大きければtrue、そうでなければfalse
	 */
	public boolean setSize(int size){
		this.assignedName = new HashSet<String>();// 変数の数を変更したら割り当て済みの変数もリセットする
		this.size = (size > 0)? size : 1;
		return (size > 0)? true : false;
	}

	/**
	 * ランダムに変数名を割り当てます。<br>
	 * 例えば割り当てる予定の変数の数が5つであれば、1回目の割り当てはaからeまでのアルファベットのうちランダムに1つ割り当てられます。<br>
	 * 2回目は1回目に割り当てられたもの以外の4つからランダムに1つ割り当てられます。<br>
	 * この場合、割り当てる予定の変数の数が５つなのでこのメソッドを使って割り当てのできる回数は5回までです。<br>
	 * 6回以降の割り当ては空文字列を返します。
	 * 
	 * @return ランダムに決定された変数名
	 */
	public String assign(){
		String name;
		do{
			name = "";
			if(this.assignedName.size() == this.size) break; // 全ての変数名が割り当てられていたら終了

			// 乱数で生成した0からsize-1までの整数を26進数に変換(26はアルファベットの総数)
			String base26 = Integer.toString((int)(Math.random() * this.size), 26);

			for(int i = 0; i < base26.length(); i++){
				String digit = String.valueOf(base26.charAt(i));                // 1文字(桁)ずつ取り出す
				name += Integer.toString(Integer.parseInt(digit, 26) + 10, 36); // 0~pをa~zに変換
			}
		}while(!this.assignedName.add(name)); // 重複していたらやり直し

		return name;
	}
}
