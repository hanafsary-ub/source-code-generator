package generate.code.graph.dependence.data;

/**
 * スクリーン上のXY座標を表現するクラスです。
 * @author 花房亮
 *
 */
public class Coordinate {
	private int x;
	private int y;

	/**
	 * x座標とy座標を初期化します。
	 * @param x x座標
	 * @param y y座標
	 */
	public Coordinate(int x, int y){
		this.x = x;
		this.y = y;
	}

	/**
	 * x座標を返します。
	 * @return x座標
	 */
	public int getX(){
		return this.x;
	}

	/**
	 * y座標を返します。
	 * @return y座標
	 */
	public int getY(){
		return this.y;
	}
}
