/**
 * データ依存グラフおよび、その構成要素を表すクラスを提供します。
 * @author 花房亮
 *
 */
package generate.code.graph.dependence.data;