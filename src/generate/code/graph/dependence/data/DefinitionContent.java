package generate.code.graph.dependence.data;

import java.util.ArrayList;
import java.util.List;

/**
 * 代入文X = YのYの部分である定義内容部を表現するクラスです。<br>
 * 変数の型を整数型に限定した場合の定義内容部は複数の変数と定数による多項式で表される。<br>
 * 例えば、以下のような代入文の場合、<br>
 * a = b + c * d / 2<br>
 * b + c * d / 2の部分が変数定義部です。
 * @author 花房亮
 *
 */
public class DefinitionContent {

	private List<VariableDefinition> operands; // 被演算子。定数も特殊な名前をもつ変数として扱う
	private List<String> operators;            // 演算子
	private int result;                        // 計算結果

	/**
	 * 変数定義部であるDefinitionContentを構築します。
	 * 
	 */
	public DefinitionContent(){
		this.operands = new ArrayList<VariableDefinition>();
		this.operators = new ArrayList<String>();
	}

	/**
	 * 演算子と被演算子から計算結果を求めます。<br>
	 * 最初の演算子は+か-のどちらかである必要があり、ゼロ割が起きないように適切な演算子が事前に設定されている必要があります。<br>
	 * また、演算子と被演算子の数は等しいことを事前に確認する必要があります。
	 * 
	 * @return 演算子と被演算子が設定されていなければfalseを反します。
	 */
	public boolean calculate(){
		boolean calculable = false;
		// 計算用の演算子と被演算子
		List<VariableDefinition> tempOperand = new ArrayList<VariableDefinition>(operands);
		List<String> tempOperator = new ArrayList<String>(operators);
		if(!(this.operands.isEmpty() || this.operators.isEmpty())){
			calculable = true;
			// 取り得ない演算子は演算子の登録の際に排除されているものとする
			// まずは優先度の高い*,/,%から計算する
			for(int i = 1; i < tempOperator.size(); i++){//最初は必ず+か-なのでiは1から始める
				// 優先度の高い*,/,%を見つけたら,i-1番目とi番目のoperandの計算結果をi番目に置き換え、i番目の演算子とi-1番目の被演算子を取り除く
				String operator = tempOperator.get(i);
				if(operator.equals("*") || operator.equals("/") || operator.equals("%")){
					VariableDefinition vd;
					if(operator.equals("*")){
						vd = new VariableDefinition(tempOperand.get(i).getName(), tempOperand.get(i - 1).getValue() * tempOperand.get(i).getValue());
					}
					else if(operator.equals("/")){
						vd = new VariableDefinition(tempOperand.get(i).getName(), (int)(tempOperand.get(i - 1).getValue() / tempOperand.get(i).getValue()));
					}
					else{//%の場合
						vd = new VariableDefinition(tempOperand.get(i).getName(), tempOperand.get(i - 1).getValue() % tempOperand.get(i).getValue());
					}
					tempOperand.set(i, vd);
					// i番目に計算を置き換えたあとはi-1番目の被演算子とi番目の演算しを取り除いてインクリメントしない
					tempOperator.remove(i);
					tempOperand.remove(i-1);
					i--;
				}
			}
			this.result = 0;
			// 残った演算子(+,-)と被演算子について計算結果を求める
			for(int i = 0; i < tempOperator.size(); i++){
				if(tempOperator.get(i).equals("+")){
					this.result += tempOperand.get(i).getValue();
				}
				else{// -の場合
					this.result -= tempOperand.get(i).getValue();
				}
			}
		}
		return calculable;
	}
	
	/**
	 * 定義内容部の式を計算した結果を返します。
	 * 
	 * @return 計算結果
	 */
	public int getResult(){
		return this.result;
	}
	
	/**
	 * 定義内容部の持つ被演算子を返します。
	 * 
	 * @return 被演算子
	 */
	public List<VariableDefinition> getOperands(){
		return this.operands;
	}

	/**
	 * 定義内容部の持つ演算子を返します
	 * 
	 * @return 演算子
	 */
	public List<String> getOperators(){
		return this.operators;
	}

}
