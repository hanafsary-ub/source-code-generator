package generate.code.graph.dependence.data;

import generate.code.io.MatrixLoader;
import generate.code.io.exception.DataDependenceException;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * データ依存グラフのすべてのノードの情報をもつクラスです。<br>
 * データ依存グラフの定義に従った隣接行列matrixが与えられている場合、以下のようにしてノードを生成することができる。<br>
 * DataDependenceGraph ddg = new DataDependenceGraph(matrix);<br>
 * ddg.generateNodes();
 * 
 * @author 花房亮
 *
 */
public class DataDependenceGraph {

	private List<Node> nodes;                         // このクラスがもつ全てのノード
	private List<String[]> adjacencyMatrix;           // 隣接行列
	private double reuseRate = 0.7;                   // コードを生成する際、変数名を決めるときに変数名を再利用する確率
	
	/**
	 * 隣接行列を指定して、データ依存グラフを構築します。<br>
	 * 隣接行列を読み込む際に、
	 * {@link MatrixLoader#checkDataDependenceException(List)}でデータ依存グラフの隣接行列として例外をチェックしています。
	 * 
	 * @param matrix データ依存グラフの隣接行列
	 * @throws DataDependenceException データ依存グラフの隣接行列の定義に関する例外
	 */
	public DataDependenceGraph(List<String[]> matrix) throws DataDependenceException{
		this.adjacencyMatrix = MatrixLoader.checkDataDependenceException(matrix);
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("./data/settings/generation.properties")); // プロパティファイルの読み込み
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.reuseRate = Double.parseDouble(prop.getProperty("reuse.rate"));
	}

	public DataDependenceGraph(int length){
		this.nodes = new ArrayList<Node>();
		for(int i = 0; i < length; i++){
			// 空のノードのリストを作成しておく
			this.nodes.add(new Node(i));
		}
	}
	/**
	 * 全てのノードをList型として返します。<br>
	 * ノードは{@link #generateNodes()}を実行する際に設定されます。
	 * 
	 * @return データ依存グラフの全てのノード
	 */
	public List<Node> getNodes(){
		return this.nodes;
	}
	
	/**
	 * このクラスの持つ隣接行列を返します。
	 * 
	 * @return データ依存グラフの隣接行列
	 */
	public List<String[]> getAdjacencyMatrix(){
		return this.adjacencyMatrix;
	}
	
	/**
	 * 隣接行列行列を元にソースコード生成に必要な情報を設定したノードを生成します。<br>
	 * これらのノードを情報を用いることでソースコードを構築することができます。<br>
	 * 実際にソースコードを表示する際には、{@link #getNodes()}でList構造で配置されたノードを取得し、
	 * {@link Node#getVariableDefinition()}と{@link Node#getDefinitionContent()}で変数定義部と定義内容部を求めます。
	 * 
	 */
	public void generateNodes(){
		this.nodes = new ArrayList<Node>();
		int variableCount = 0; // 変数の数を数え上げる
		for(int i = 0; i < this.adjacencyMatrix.size(); i++){

			Node node = new Node(i);
			/*変数定義部を決定する*/
			variableCount = this.assignTemporaryVariableNames(node, variableCount); // 仮の変数名を決めておく
			/*定義内容部を決定する*/
			DefinitionContent dc = new DefinitionContent();
			this.assignOperands(dc, i);           // 被演算子を決める
			Collections.shuffle(dc.getOperands()); // DefinitionContentの項の順番をランダムに入れ替える
			this.assignOperators(dc);             // 演算子を決める
			node.setDefinitionContent(dc);

			/*ノードiの変数定義部の値を計算する*/
			dc.calculate();
			node.getVariableDefinition().setValue(dc.getResult());

			this.nodes.add(node); 

		}
		//ランダムな変数名を割り当てる。定数の名前を数値に置き換える
		this.assignNames(variableCount);
	}

	/**
	 * ノード番号を仮の変数名として付ける。隣接行列を見て、再利用可能であればランダムに再利用するか決定する。
	 * どの変数名を再利用するかもランダムに決定する。
	 * 
	 * @param node i番目のノード
	 * @param variableCount 現在の変数の総数
	 * @return 更新された変数の総数
	 */
	private int assignTemporaryVariableNames(Node node, int variableCount){
		int count = variableCount;
		List<Integer> reuseableID = new ArrayList<Integer>();
		
		int nodeID = node.getID();
		// ノードi(nodeID)の変数定義部の変数名が他のノードのもので代用できるか確認する
		for(int j = 0; j <= nodeID - 1; j++){
			// もし再利用可能なら変数名が再利用可能なノードとして加える
			if(this.isReuseable(nodeID, j)){
				reuseableID.add(j);
			}
		}

		int size = reuseableID.size();
		// もし再利用可能な変数名が存在すれば一定の確率で再利用を決定する
		if(size > 0 && Math.random() < this.reuseRate){
			int reuseID = reuseableID.get((int)(Math.random() * size)); // 再利用可能なものの中からランダムに選ぶ
			// 変数名を再利用する
			String reuseName = nodes.get(reuseID).getVariableDefinition().getName();
			node.setVariableDefinition(new VariableDefinition(reuseName));
		}else{
			// 仮の変数名を付ける
			node.setVariableDefinition(new VariableDefinition(Integer.toString(variableCount)));
			count++;
		}
		return count;
	}

	/**
	 * ノードi(targetID)に対して、既に変数名が決められている変数をノード(reuseID)の変数名を使いまわしても問題ないか確認する。
	 * 変数名が再利用できる変数は、既にプログラム文が生成されたノードの中で、以降参照されることのないノードの変数である。
	 * 具体的には、ノードi(targetID)の変数名を求める際に、i+1番目以降のすべてのノードに参照されることのないi-1番目以前のノードの変数を再利用することができる。
	 * この最初のノードからi-1番目のノードまでのうちのとあるノードをreuseID番目のノードとしている。
	 * また一番最後のノードが参照されることはないので、一番最後のノードはすべてのノードを再利用することができる。
	 * 
	 * @param targetID 変数名を定義するノードのID
	 * @param reuseID  targetIDのノードに再利用できるかどうか確認するノードのID
	 * @return reuseIDのノードの変数名をtargetIDのノードにも使えるならtrue、そうでないならfalse
	 */
	private boolean isReuseable(int targetID, int reuseID){
		boolean reuseable = true;
		// targetIDのノードが最後のノードであれば必ず再利用可能
		if(targetID != this.adjacencyMatrix.size() - 1){
			for(int i = targetID + 1; i < this.adjacencyMatrix.size(); i++){
				int element = Integer.parseInt(this.adjacencyMatrix.get(reuseID)[i]);
				if(element == 1){
					reuseable = false;
					break;
				}
			}
		}
		return reuseable;
	}

	/**
	 * 定義内容部の被演算子について、隣接行列行列に基いて求める。
	 * 有向グラフの隣接行列において、あるノードiに対して隣接行列を行方向に見れば(i行を見れば)、どのノード(どの列)にエッジが伸びているか見ることができる。
	 * また、あるノードiに対して隣接行列を列方向に見れば(i列を見れば)、どのノード(どの行)からエッジが伸びているか見ることができる。
	 * このノードiに対して参照する変数を求めるには隣接行列を列方向に見て値が1の要素があるか見ればよいことがわかる。
	 * また、どの変数も参照しないノードは定数で初期化しないといけないので、必ず１つ定数を追加するようにしている。
	 * 
	 * @param definitionContent i番目のノードの定義内容部
	 * @param nodeID ノードiのノード番号
	 */
	private void assignOperands(DefinitionContent definitionContent, int nodeID){
			for(int j = 0; j < this.adjacencyMatrix.size(); j++){
				//ノードiについて隣接行列を列方向に見る
				int element = Integer.parseInt(this.adjacencyMatrix.get(j)[nodeID]);
				if(element == 1){ // 1ならj番目の変数を変数定義部に追加する
					definitionContent.getOperands().add(this.nodes.get(j).getVariableDefinition());
				}
			}
			// 定数の追加
			VariableDefinition constant = new VariableDefinition("CONSTANT");
			// 定数の値は0から9ランダム
			constant.setValue((int)(Math.random() * 10));
			definitionContent.getOperands().add(constant);
		
	}

	/**
	 * 定義内容部の被演算子に基いて演算子をランダムに決める。
	 * 0番目の演算子は先頭の変数(定数)の前に付く演算子なので、+か-しか取り得ない
	 * また、ゼロ除算が起こる位置に/または%が割り当てることは避けるべきであるので、取りうる演算子は/,%を除いたものになる。
	 * 
	 * @param definitionContent i番目のノードの定義内容部
	 */
	private void assignOperators(DefinitionContent definitionContent){
		List<String> operators = definitionContent.getOperators();
		List<VariableDefinition> operands = definitionContent.getOperands();
		for(int i = 0; i < operands.size(); i++){
			// 選択可能な演算子のリスト。状況に応じて使えない演算子は除外して、残った演算子の中からランダムに選ぶ
			// 除外する理由は被験者に必要以上の暗算の負荷を与えたくないため。
			// 除外理由の詳細 → https://www.evernote.com/l/ABxPMk20NzNEvJjaPHtrfhUoxderPJCuRdU
			List<String> op = new ArrayList<>(Arrays.asList("+", "-", "*", "/", "%"));
			if(i == 0){//最初の項なら+か-以外を除外する
				op.remove("*");
				op.remove("/");
				op.remove("%");
			}
			else{// 最初の項でない場合
				// 一つ前の演算子が*, /, %のいずれかだったら次は必ず+か-でなければならない
				if(operators.get(i-1).equals("*") || operators.get(i-1).equals("/") || operators.get(i-1).equals("%")){
					op.remove("*");
					op.remove("/");
					op.remove("%");
				}else{// 1つ前が+か-だった場合
					// 2番目の項で最初の項が-だったら余りを計算しない。
					if(i == 1 && operators.get(0).equals("-")){
						op.remove("%");
					}
					// 掛け算は一桁同士でなければ選択されない
					if(operands.get(i-1).getValue() > 9 || operands.get(i).getValue() > 9){
						op.remove("*");
					}
					if(operands.get(i).getValue() == 0){//ゼロ除算が起きる場合は'/'と'%'を除外する
						op.remove("/");
						op.remove("%");
					}
					else{// ゼロ除算が起きない
						//割り切れない場合除算は選択されない
						if(operands.get(i-1).getValue() % operands.get(i).getValue() != 0){
							op.remove("/");
						}
						// 余りは1より大きい数同士でなければならない。さらに先の項が後の項よりも大きいか等しくなければならない。
						if(operands.get(i-1).getValue() <= 0 || operands.get(i).getValue() <= 0 ||
								operands.get(i-1).getValue() < operands.get(i).getValue()){
							op.remove("%");
						}
					}
				}
			}
			// 選択可能な演算子の中から１つランダムに選ぶ
			int r = (int)(Math.random() * op.size());
			operators.add(op.get(r));
		}
	}

	/**
	 * すべてのノードにおいて仮の変数名をランダムな変数名に置き換える。
	 * 置き換える前は名前を割り当てた順に仮の変数名が割り当てられていて、定数はCONSTANTという変数名をとる特殊な変数として設定されている。
     * 定義内容部の項で参照されている変数はVariableDefinitionをそのまま代入文しているので、変数定義部の変数名を変更するだけで自動的に反映される。
     * 
	 * @param variableCount 使用される変数の数
	 */
	private void assignNames(int variableCount){
		VariableNameGenerator generator = new VariableNameGenerator(variableCount);
		for(int i = 0; i < variableCount; i++){
			String name = generator.assign();
			for(int j = 0; j < this.nodes.size(); j++){
				VariableDefinition vd = this.nodes.get(j).getVariableDefinition();
				if(vd.getName().equals(Integer.toString(i))){
					vd.setName(name);
				}
			}
			
		}
		// 定数の名前を数値に置き換える
		for(int i = 0; i < this.nodes.size(); i++){
			List<VariableDefinition> operands = this.nodes.get(i).getDefinitionContent().getOperands();
			for(VariableDefinition vd : operands){
				if(vd.getName().equals("CONSTANT")){
					vd.setName(Integer.toString(vd.getValue()));
				}
			}
		}
		
	}
}
