package generate.code.io;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 複数のデータ形式に対応した視線データクラスです。<br>
 * 現在はTobiiアイトラッカーから出力されたTSVファイルか、TheEyeTribeから出力されたカンマ区切りのDATファイルに対応しています。<br>
 * このクラスで表現された視線データは、タイムスタンプとX座標、Y座標の３つのカラムで構成されています。
 * 
 * @author 花房亮
 *
 */
public class EyeTrackingData {
	/**
	 * Tobii アイトラッカーから出力されたTSVファイルであることを示します。
	 */
	public static final int TOBII_TSV = 0;

	/**
	 * The Eye Tribeから出力されたカンマ区切りのDATファイルであることを示します。
	 */
	public static final int EYETRIBE_DAT = 1;

	private int dataType;                                            // TobiiアイトラッカーのTSVなら0、TheEyeTribeのDATなら1
	private List<String[]> rowSplitData = new ArrayList<String[]>(); // カラムタイトル(ラベル)を除いたデータ。時系列ごとに格納されている
	private List<List<String>> columnSplitData = new ArrayList<>();  // カラムタイトルごとに格納されたデータ
	private int rowSize;                                             // このデータの行数
	private int xIndex = -1;                                         // 視線座標のx座標が書いてある列の添字
	private int yIndex = -1;                                         // 視線座標のy座標が書いてある列の添字
	
	/**
	 * データ形式と入力視線データを指定してEyeTrackingDataを構成します。<br>
	 * inputDataの各行は文字列配列であることに注意してください。
	 * 
	 * @param dataType 0ならTobiiアイトラッカーのTSVファイル、1ならTheEyeTribeのDATファイル
	 * @param inputData 各行が文字列配列の入力視線データ
	 */
	public EyeTrackingData(int dataType, List<String[]> inputData){
		this.dataType = dataType;
		String[] labels = inputData.get(0); // inputDataの1行目はデータのラベルになっている
		this.rowSize = inputData.size();
		
		Pattern px = Pattern.compile("PointX");
		Pattern py = Pattern.compile("PointY");
		/* 視線のx座標やy座標の書いてある列を記憶しておく */
		for(int i = 0; i < labels.length; i++){
			columnSplitData.add(new ArrayList<>());
			this.columnSplitData.get(i).add(labels[i]);
			if(px.matcher(labels[i]).find()){
				xIndex = i;
			}
			if(py.matcher(labels[i]).find()){
				yIndex = i;
			}
		}
		
		/* inputDataを行ごとに分割したrowSplitDataと、列ごとに分割したcolumnSplitDataとして持っておく */
		for(int i = 1; i < inputData.size(); i++){
			String[] row = inputData.get(i);
			this.rowSplitData.add(row);
			for(int j = 0; j < labels.length; j++){
				this.columnSplitData.get(j).add(row[j]);
			}
		}

	}
	
	
	public List<String> convertToLines(){
		List<String> text = new ArrayList<>();
		
		for(int i = 0; i < this.rowSize; i++){
			List<String> newCol = new ArrayList<>();
			for(List<String> col : this.columnSplitData){
				newCol.add(col.get(i));
			}
			text.add(String.join(",", newCol));
		}
		return text;
	}
	/**
	 * この視線データのデータ形式を返します。
	 * @return 0ならTobiiアイトラッカーのTSVファイル、1ならTheEyeTribeのDATファイルを表すデータ形式
	 */
	public int getDataType(){
		return this.dataType;
	}
	
	/**
	 * カラムタイトルを除く視線データの内容を返します。
	 * @return カラムタイトルを除く視線データの内容
	 */
	public List<String[]> getContent(){
		return this.rowSplitData;
	}

	public List<List<String>> getColumnSplitData(){
		return this.columnSplitData;
	}
	public int getRowSize(){
		return this.rowSize;
	}
	public int getXIndex(){
		return this.xIndex;
	}
	public int getYIndex(){
		return this.yIndex;
	}
}
