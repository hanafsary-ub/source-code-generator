package generate.code.io.exception;

/**
 * 入力視線データが正しいカラムを持っていない場合にスローされる例外です。
 * @author 花房亮
 *
 */
public class TrackingDataException extends Exception {

	/**
	 * 自動生成されたシリアル・バージョンID
	 */
	private static final long serialVersionUID = 7683031320088084205L;

	/**
	 * 詳細メッセージを指定しないでTrackingExceptionを構築します。
	 */
	public TrackingDataException(){
		super();
	}

	/**
	 * 指定された詳細メッセージを持つTrackingDataExceptionを構築します。
	 * @param message 詳細メッセージ
	 */
	public TrackingDataException(String message){
		super(message);
	}
}
