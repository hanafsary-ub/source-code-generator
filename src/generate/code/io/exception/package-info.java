/**
 * 入力ファイルの形式に関する例外クラスを提供します。
 * @author 花房亮
 *
 */
package generate.code.io.exception;