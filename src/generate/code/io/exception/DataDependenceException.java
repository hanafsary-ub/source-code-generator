package generate.code.io.exception;

/**
 * 入力隣接行列がデータ依存グラフとして正しい型でない場合にスローされる例外です。
 * @author 花房亮
 *
 */
public class DataDependenceException extends Exception {

	/**
	 * 自動生成されたシリアル・バージョンID
	 */
	private static final long serialVersionUID = 8737019312901410722L;

	/**
	 * 指定された詳細メッセージを持つDetaDependenceExceptionを構築します。
	 * @param message 詳細メッセージ
	 */
	public DataDependenceException(String message){
		super(message);
	}
}
