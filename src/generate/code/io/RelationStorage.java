package generate.code.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import generate.code.graph.dependence.data.Coordinate;
import generate.code.graph.dependence.data.DataDependenceGraph;
import generate.code.graph.dependence.data.LineRange;
import generate.code.graph.dependence.data.Node;

/**
 * 生成ソースコードの情報などをデータベースを通して管理するクラス。<br>
 * 
 * @author 花房亮
 *
 */
public class RelationStorage {
	private int generatedCodeID = -1;
	/**
	 * データベースから生成ソースコードIDを新たに生成し返します。
	 * 
	 * @return 生成ソースコードのID
	 */
	public int getNewID(){
		int newID = -1;
		DatabaseConnection.connect();
		Connection conn = DatabaseConnection.getConnection();
		try {
			Statement statement = conn.createStatement();
			ResultSet rs = statement.executeQuery("SELECT COUNT(*) FROM SourceCode;");
			newID = (rs.next())? rs.getInt(1) : 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DatabaseConnection.disconnect();
		return newID;
	}

	public void updateLineRange(DataDependenceGraph graph){
		int size = this.fetchSize();
		DatabaseConnection.connect();
		Connection conn = DatabaseConnection.getConnection();
		try{
			String update = "UPDATE LineRange SET leftTopX = ?, leftTopY = ?, width = ?, height = ? WHERE generatedCodeID = " + generatedCodeID + " AND lineRangeID = ?;";
			PreparedStatement ps = conn.prepareStatement(update);

			for(int i = 0; i < size; i++){
				LineRange range = graph.getNodes().get(i).getRange();
				ps.setInt(1, range.getTopLeft().getX());
				ps.setInt(2, range.getTopLeft().getY());
				ps.setInt(3, range.getWidth());
				ps.setInt(4, range.getHeight());
				ps.setInt(5, i);
				ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DatabaseConnection.disconnect();
	}
	/**
	 * データ依存グラフと生成ソースコードをデータベースに保存します。
	 * 
	 * @param graph データ依存グラフ
	 * @param layout 生成ソースコード
	 */
	public void save(DataDependenceGraph graph, SourceCodeLayout layout){
		DatabaseConnection.connect();
		Connection conn = DatabaseConnection.getConnection();
		//生成コードIDを求める
		int codeID = layout.getID();

		// 隣接行列をカンマ区切りの1行の文字列に変換する
		List<String[]> matrix = graph.getAdjacencyMatrix();
		String oneLine = matrix.stream().map(line -> String.join(",", line)).collect(Collectors.joining(","));

		try {
			String insertion1 = "INSERT INTO SourceCode VALUES(?,?,?);";
			String insertion2 = "INSERT INTO LineRange  VALUES(?,?,?,?,?,?);";
			String insertion3 = "INSERT INTO Node       VALUES(?,?,?);";
			PreparedStatement ps1 = conn.prepareStatement(insertion1);
			ps1.setInt(1, codeID); ps1.setString(2, oneLine); ps1.setInt(3, matrix.size());
			ps1.executeUpdate();
			PreparedStatement ps2 = conn.prepareStatement(insertion2);
			PreparedStatement ps3 = conn.prepareStatement(insertion3);
			for(Node node : graph.getNodes()){
				LineRange range = node.getRange();
				ps2.setInt(1, codeID);
				ps2.setInt(2, node.getID());
				ps2.setInt(3, range.getTopLeft().getX());
				ps2.setInt(4, range.getTopLeft().getY());
				ps2.setInt(5, range.getWidth());
				ps2.setInt(6, range.getHeight());

				ps3.setInt(1, codeID);
				ps3.setInt(2, node.getID());
				ps3.setString(3, layout.getCode()[node.getID()]);

				ps2.addBatch();
				ps3.addBatch();
			}
			ps2.executeBatch();
			ps3.executeBatch();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DatabaseConnection.disconnect();
	}
	
	/**
	 * ノードの画面上の範囲をデータベースから読み込みます。
	 * 
	 * @param generatedCodeID 生成ソースコードID
	 * @return すべてのノードの範囲
	 */
	public List<LineRange> loadRange(){
		List<LineRange> ranges = new ArrayList<LineRange>();
		DatabaseConnection.connect();
		Connection conn = DatabaseConnection.getConnection();
		try {
			Statement statement = conn.createStatement();
			String sql = "SELECT * FROM LineRange WHERE generatedCodeID = " + this.generatedCodeID + " ORDER BY lineRangeID;";
			ResultSet rs = statement.executeQuery(sql);
			while(rs.next()){
				int x = rs.getInt(3);
				int y = rs.getInt(4);
				Coordinate topLeft = new Coordinate(x, y);
				int width = rs.getInt(5);
				int height = rs.getInt(6);
				ranges.add(new LineRange(topLeft, width, height));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DatabaseConnection.disconnect();
		return ranges;
	}
	
	public void findID(File file){
		this.generatedCodeID = -1;
		String line;
		/* HTMLファイルの中から生成ソースコードのIDの部分を見つけて抽出する */
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			while((line = br.readLine()) != null){
				Pattern p = Pattern.compile("(?<=<!--生成コードID:)\\d+");
				Matcher m = p.matcher(line);
				if(m.find()){
					generatedCodeID = Integer.parseInt(m.group());
					break;
				}
			}
			br.close();
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
		//TODO:generatedCodeIDがHTMLファイル内に記述されていない場合の例外処理を書く
		//return this.loadRange(generatedCodeID);
	}
	
	public int getGeneratedCodeID(){
		return this.generatedCodeID;
	}
	
	/**
	 * ソースコードの行数をデータベースから取得します。
	 * @return ソースコードの行数
	 */
	public int fetchSize(){
		int size = 0;
		DatabaseConnection.connect();
		Connection conn = DatabaseConnection.getConnection();
		try {
			Statement statement = conn.createStatement();
			String sql = "SELECT * FROM SourceCode WHERE generatedCodeID = " + generatedCodeID + ";";
			ResultSet rs = statement.executeQuery(sql);
			if(rs.next()){
				size = rs.getInt(3);
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DatabaseConnection.disconnect();
		return size;
	}
}
