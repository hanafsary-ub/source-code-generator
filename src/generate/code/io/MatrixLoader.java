package generate.code.io;

import generate.code.io.exception.DataDependenceException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * CSVファイルから隣接行列を読み込み、データ依存グラフの定義に沿っているか確認する機能を持つクラスです。<br>
 * 主な使用例は以下の通りです。<br>
 * File file = new File("c://hoge/abc.csv);<br>
 * List&lt;String[]&gt; matrix = MatrixLoader.readCSV(file);<br>
 * try{<br>
 *     matrix = MatrixLoader.checkDataDependenceException(matrix);<br>
 * }catch(DataDependenceException e){<br>
 *     e.printStackTrace();<br>
 * }<br><br>
 * また、以下のようにまとめて書くこともできます。<br>
 * try{<br>
 *     matrix = MatrixLoader.checkDataDependenceException(MatrixLoader.readCSV(new File("c://hoge/abc.csv)));
 * }catch(DataDependenceException e){<br>
 *     e.printStackTrace();<br>
 * }<br><br>
 * 
 * @author 花房亮
 *
 */
public class MatrixLoader {

	/**
	 * CSVファイルを読み込み、隣接行列の元となる２次元の行列を得ることができます。<br>
	 * 入力CSVファイルの行頭に#が書かれていたらコメント行とみなし読み飛ばします。
	 * 
	 * @param file 入力CSVファイル
	 * @return CSVファイルから読み込んだ行列
	 */
	public static List<String[]> readCSV(File file){
		List<String[]> matrix = new ArrayList<String[]>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = "";
			while((line = br.readLine()) != null){
				// 行頭が#だったらコメント文とみなし読み飛ばす
				if(line.charAt(0) == '#') continue;
				matrix.add(line.split(","));
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return matrix;
	}
	
	/**
	 * 入力の隣接行列がデータ依存グラフの隣接行列の定義にそぐわない場合に例外をスローします。<br>
	 * 例外がなければ隣接行列をそのまま返します。<br>
	 * データ依存グラフの隣接行列の定義は以下の通りです。<br><br>
	 * 0. 有向グラフの隣接行列であること(正方行列でエッジが引かれている場合1、引かれない場合0を取る)<br>
	 * 1. 対角成分と下三角成分はすべて0<br>
	 * 2. i行(j列)はノード番号に等しく、ノード番号は生成されるコードの行番号に等しい(これはどちらかというと1.のための前提条件)
	 * 
	 * @param matrix 例外の確認が済んでいない隣接行列
	 * @return 例外が確認されなかった隣接行列
	 * @throws DataDependenceException データ依存グラフの隣接行列の定義にそぐわない場合の例外
	 */
	public static List<String[]> checkDataDependenceException(List<String[]> matrix) throws DataDependenceException{
		// i行j列の二重ループ
		for(int i = 0; i < matrix.size(); i++){

			String[] line = matrix.get(i);
			// 行の大きさ(列数)が１つでも行数に一致しなければ正方行列とみなさない
			if(line.length != matrix.size()){
				throw new DataDependenceException("入力データが正方行列でない可能性があります。");
			}
			for(int j = 0; j < line.length; j++){
				int element = Integer.parseInt(line[j]);
				// データ依存グラフの隣接行列は対角成分と下三角成分すべてが0でなければならない
				if(j <= i && element != 0){
					throw new DataDependenceException("対角成分および下三角成分に0でない成分が含まれています。");
				}
				// その他の行列の成分は1(ノードiからノードjへエッジが引かれている場合)または0(エッジが引かれていない場合)でなければならない
				else if(!(element == 0 || element == 1)){
					throw new DataDependenceException("隣接行列の要素に0または1でないものが含まれています。");
				}
			}

		}
		// 例外がなければそのまま返す。
		return matrix;
	}
}
