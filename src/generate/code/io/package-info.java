/**
 * ファイルの入出力および、データベースへの読み込み/書き込みを行うクラスを提供します。
 * @author 花房亮
 *
 */
package generate.code.io;