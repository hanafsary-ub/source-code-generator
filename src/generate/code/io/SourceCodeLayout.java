package generate.code.io;

import generate.code.graph.dependence.data.DataDependenceGraph;
import generate.code.graph.dependence.data.DefinitionContent;
import generate.code.graph.dependence.data.Node;
import generate.code.graph.dependence.data.VariableDefinition;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * データ依存グラフの情報からソースコードを生成し、HTML形式に成形するクラスです。<br>
 * 出力先フォルダはdata_path.propertiesファイルのpath.output.htmlに設定します。<br>
 * HTMLの書式はhtml_format.propertiesで設定します。出力文字のフォントサイズはformat.font.size、行間の間隔はformat.line.height、
 * 上部の余白はformat.margin.top、左側の余白はformat.margin.leftで設定します。<br>
 * このクラスを利用する上での注意点としては、{@link #getCode()}や{@link #printToHTML(String)}を利用する上では、
 * その前に{@link #buildCode()}を実行しておく必要があります。
 * 
 * @author 花房亮
 *
 */
public class SourceCodeLayout {

	private int id;                         // 生成されたソースコードのID
	private List<Node> nodes;               // ソースコード生成のための情報を持ったデータ依存グラフのノード
	private String[] sourceCode;            // 配列で1行毎に格納した生成ソースコード
	private String path;                    // 出力フォルダのパス
	private List<String[]> adjacencyMatrix; // HTMLにコメント文で書き込む生成元の隣接行列
	private int fontSize;                   // HTMLファイルにおける文字のフォントサイズ
	private int lineHeight;                 // HTMLファイルにおける行間
	private int marginTop;                  // HTMLファイルにおける左側の余白
	private int marginLeft;                 // HTMLファイルにおける上部の余白
	private Properties prop = new Properties();

	/**
	 * データ依存グラフの情報を元にSourceCodeLayoutを構築します。<br>
	 * また、data_path.propertiesファイルからHTMLファイルの出力先フォルダ、html_format.propertiesから出力HTMLファイルの書式設定を読み込みます。
	 * 
	 * @param graph 生成コードの情報を持ったデータ依存グラフ
	 */
	public SourceCodeLayout(DataDependenceGraph graph){
		this.nodes = graph.getNodes();
		this.adjacencyMatrix = graph.getAdjacencyMatrix();
		this.sourceCode = new String[nodes.size()];
		try {
			prop.load(new FileInputStream("./data/settings/data_path.properties")); // プロパティファイルの読み込み
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.path = prop.getProperty("path.output.html");
		try {
			prop.load(new FileInputStream("./data/settings/html_format.properties")); // プロパティファイルの読み込み
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.fontSize = Integer.parseInt(prop.getProperty("format.font.size"));
		this.lineHeight = Integer.parseInt(prop.getProperty("format.line.height"));
		this.marginTop = Integer.parseInt(prop.getProperty("format.margin.top"));
		this.marginLeft = Integer.parseInt(prop.getProperty("format.margin.left"));

	}
	
	
	/**
	 * html_format.propertiesの設定を読み込みます。
	 */
	public void reloadHTMLFormat(){
		try {
			prop.load(new FileInputStream("./data/settings/html_format.properties")); // プロパティファイルの読み込み
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.fontSize = Integer.parseInt(prop.getProperty("format.font.size"));
		this.lineHeight = Integer.parseInt(prop.getProperty("format.line.height"));
		this.marginTop = Integer.parseInt(prop.getProperty("format.margin.top"));
		this.marginLeft = Integer.parseInt(prop.getProperty("format.margin.left"));
	}
	
	/**
	 * データ依存グラフのノードの情報を元にソースコードを生成します。<br>
	 * また、文字列としてのソースコードを表現する際に不要な情報をノードの情報から省略します。<br>
	 * 例えば、代入文を表現するノードの右辺(定義内容部)の多項式では、必ず先頭の演算子に+か-が付いています。
	 * 先頭の演算子が+の場合や、先頭の被演算子が定数の0で演算子が-の場合には演算子の省略を行っています。<br>
	 * 他にも末尾の演算子と被演算子(定数)の組み合わせが+0,-0,*1,/1の場合にはその組み合わせの省略を行っています。<br>
	 */
	public void buildCode(){
		for(Node node : this.nodes){
			String sentence = "";
			sentence += node.getVariableDefinition().getName() + " =";
			DefinitionContent dc = node.getDefinitionContent();
			List<VariableDefinition> operands = dc.getOperands();
			List<String> operators = dc.getOperators();
			// 特に先頭の項とその周りに関して、省略できる演算子は省略する
			if(operators.get(0).equals("+") || // 先頭の+は必ず省略
			  (operators.get(0).equals("-") && operands.get(0).getName().equals("0"))) // - 0 ... の場合
			{ 
				sentence += " " +operands.get(0).getName(); // 先頭の演算子を省略
			}else{// それ以外は省略しない
				sentence += " " + operators.get(0) + operands.get(0).getName();
			}
			// １版最後の演算子と項を残して２番めの演算子から書いていく
			for(int i = 1; i < operators.size() - 1; i++ ){
				sentence += " " + operators.get(i) + " " + operands.get(i).getName();
			}
			// もし最後の演算子と被演算子の組み合わせが省略できるものであったら省略する
			int lastIndex = operators.size() - 1;
			if(!((operators.get(lastIndex).equals("+") && operands.get(lastIndex).getName().equals("0")) || // ... + 0
			     (operators.get(lastIndex).equals("-") && operands.get(lastIndex).getName().equals("0")) || // ... - 0
			     (operators.get(lastIndex).equals("*") && operands.get(lastIndex).getName().equals("1")) || // ... * 1
			     (operators.get(lastIndex).equals("/") && operands.get(lastIndex).getName().equals("1")) )) // ... / 1 は省略する(それ以外の場合は省略しない)
			{
				if(operators.size() != 1){
					sentence += " " + operators.get(lastIndex) + " " + operands.get(lastIndex).getName();
				}
			}
			sourceCode[node.getID()] = sentence;
		}
	}
	
	/**
	 * 生成されたソースコードを1行ごとに配列に入れたString型配列で返します。<br>
	 * ソースコードを取得するにはこのメソッドを呼び出す前に、 {@link #buildCode()}を実行しておく必要があります。
	 * 
	 * @return プログラム文ごとに配列に入れた生成ソースコード
	 */
	public String[] getCode(){
		return this.sourceCode;
	}
	
	/**
	 * ファイル名を指定して生成したソースコードをHTML形式で出力します。<br>
	 * ファイル名は拡張子を含みません。出力ファイルの出力先フォルダはdata_path.propertiesファイルのpath.output.htmlに設定されています。<br>
	 * 出力HTMLファイルの書式設定はhtml_format.propertiesに設定されています。<br>
	 * また、HTMLファイルにはコメント文として生成元の隣接行列が挿入されます。<br>
	 * ソースコードを出力するにはこのメソッドを呼び出す前に、 {@link #buildCode()}を実行しておく必要があります。
	 * 
	 * @param fileName 拡張子を含まないファイル名
	 */
	public void printToHTML(String fileName){
		//TODO:フォントサイズや行間などを自動調整できるようにする(先にスクリーンショットを取って表示部分の領域の大きさを取得する必要がある？)
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(this.path + "/" + fileName + ".html")));
			this.id = new RelationStorage().getNewID();
			bw.write("<!--生成コードID:" + id);bw.newLine();// 生成したソースコードのIDを入れておく
			bw.write("生成元の隣接行列");bw.newLine();
			for(String[] line : this.adjacencyMatrix){
				bw.write(String.join(",", line));
				bw.newLine();
			}
			bw.write("-->");              bw.newLine();
			bw.write("<html>");           bw.newLine();
			bw.write("  <head>");         bw.newLine();
			bw.write("    <style type=\"text/css\">");
			bw.newLine();
			bw.write("<!--");             bw.newLine();
			// line-heightとfont-sizeが等しいなら行間の余白は0だから、cssでの行間はフォントサイズ(px)を含めたものになる
			bw.write("body{line-height: " + (this.lineHeight + this.fontSize) + "px;font-size: " + this.fontSize + "px}");
			bw.newLine();
			bw.write("-->");              bw.newLine();
			bw.write("    </style>");     bw.newLine();
			bw.write("  </head>");        bw.newLine();
			bw.write("  <body>");         bw.newLine();
			bw.write("    <div style=\"margin-top:" + this.marginTop + "px;margin-left:" + this.marginLeft + "px\">");
			bw.newLine();
			bw.newLine();
			for(String line : this.sourceCode){
				bw.write(line + "<br/>");
				bw.newLine();
			}
			bw.write("    </div>");       bw.newLine();
			bw.write("  </body>");        bw.newLine();
			bw.write("</html>");          bw.newLine();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 生成したソースコードのIDを返します。
	 * 
	 * @return 生成したソースコードのID
	 */
	public int getID(){
		return this.id;
	}
}
