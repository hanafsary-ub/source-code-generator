package generate.code.io;
import static generate.code.io.EyeTrackingData.EYETRIBE_DAT;
import static generate.code.io.EyeTrackingData.TOBII_TSV;
import generate.code.graph.dependence.data.LineRange;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 画面上のXY座標で表された視線データをノードごとの推移に変換するクラスです。<br>
 * {@link #readTSV(File)}でファイルを読み込んで、その戻り値の結果から変換するかしないか決めてください。<br>
 * nullデータあるいは範囲外の視線データを使用するかは、node_transition.propertiesファイルで設定してください。
 * 
 * @author 花房亮
 *
 */
public class EyeTrackingAnalyzer {

	private List<String> outputData = new ArrayList<String>();
	private EyeTrackingData data;


	/**
	 * 時間と視線の座標の書かれたTSV形式のファイルを読み込みます。<br>
	 * device.propertiesのdevice.numberが0のとき、すなわちTobiiアイトラッカーの出力データを読み込むときに呼び出されます。<br>
	 * 
	 * @param file 入力TSVファイル
	 * @return ノードの推移に変換できるならtrue、そうでないならfalse
	 */
	public boolean readTSV(File file){
		boolean convertable =false;
		List<String[]> inputData = new ArrayList<String[]>(); // 入力ファイルの視線データをリストに格納したもの
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = br.readLine();          // 1行目を読み込む
			line = line.substring(1, line.length()); // Tobiiの出力ファイルの1行目の1文字目には文字化けする不要な文字が入っているため除外する

			// 入力データにGazePointXとGazePointYまたはFixationPointXとFixationPointYが含まれているかどうか
			Pattern p = Pattern.compile("^(?=.*PointX \\((ADCSpx|MCSpx)\\))(?=.*PointY \\((ADCSpx|MCSpx)\\))");
			if(p.matcher(line).find()){
				convertable = true;
				inputData.add(line.split("\t"));
				while((line = br.readLine()) != null){
					String[] columns = line.split("\t");
					inputData.add(columns);
				}
				this.data = new EyeTrackingData(TOBII_TSV, inputData);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return convertable;
	}
	
	/**
	 * 視線の座標の書かれたDAT形式のファイルを読み込みます。<br>
	 * device.propertiesのdevice.numberが1のとき、すなわちTheEyeTribeの出力データを読み込むときに呼び出されます。<br>
	 * 
	 * @param file 入力DATファイル
	 * @return ノードの推移に変換できるならtrue、そうでないならfalse
	 */
	public boolean readDAT(File file){
		boolean convertable = false;
		List<String[]> inputData = new ArrayList<String[]>(); // 入力ファイルの視線データをリストに格納したもの
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String[] columns = {"TimeStamp(null)", "GazePointX", "GazePointY"}; // カラムタイトル
			if(br.readLine().split(",").length == 2){ // 1行読み込んで2列なら
				convertable = true;
				br.close();
				br = new BufferedReader(new FileReader(file)); // BufferedReaderを初期化する
				inputData.add(columns);
				String line;
				while((line = br.readLine()) != null){
					columns = line.split(",");
					String[] newCol = {"null", columns[0], columns[1]}; // TSVのカラム数に合わせる
					inputData.add(newCol);
				}
				this.data = new EyeTrackingData(EYETRIBE_DAT, inputData);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return convertable;
	}
	
	/**
	 * 視線データをEyeTrackingDataの形式で返します。
	 * @return EyeTrackingDataの形式で表現された視線データ
	 */
	public EyeTrackingData getEyeTrackingData(){
		return this.data;
	}
	
	/**
	 * {@link #toNodeTransition(List)}で変換した出力用データを返します。
	 * 
	 * @return {@link #toNodeTransition(List)}で変換したList構造の出力用データ
	 */
	public List<String> getOutputData(){
		return outputData;
	}
	
	/**
	 * 視線の推移を画面上のx,y座標からどの行を見ているかに変換します。<br>
	 * 1行目がnullでない視線データのうちいづれかの行の範囲に含まれる視線の比率、2行目がカラム名で3行目から実際のデータで構成されます。<br>
	 * 1列目がタイムスタンプ、２列目がノード番号(行番号)です。<br>
	 * nullデータはノード番号の代わりにnullと表示され、範囲外のデータはOut of Rangeと表示されます。
	 * 
	 * @param ranges プログラムの行が画面上のどの範囲にあるかを示したリスト
	 */
	public void toNodeTransition(List<LineRange> ranges){
		int total = 0, within = 0;
		List<String> pointX = data.getColumnSplitData().get(data.getXIndex());
		List<String> pointY = data.getColumnSplitData().get(data.getYIndex());
		List<String> lineNumber = new ArrayList<>();
		String title = new String(pointX.get(0)).replace("PointX", "LineNumber"); // 行番号用のカラムタイトルを用意する
		lineNumber.add(title);
		for(int i = 1; i < data.getRowSize(); i++){
			// Tobiiアイトラッカーの視線データの座標が取得できなかったデータ(null)が含まれている場合 
			if(pointX.get(i).matches(".*null")){
				lineNumber.add("null");
				continue;
			}
			total++; // 推移量の総量を数える
			boolean anyWithin = false;
			// 視線の座標がどの範囲内にあるか探索する 
			for(int j = 0; j < ranges.size(); j++){
				LineRange range = ranges.get(j);
				int x = Integer.parseInt(pointX.get(i));
				int y = Integer.parseInt(pointY.get(i));
				if(range.isWithin(x, y)){
					lineNumber.add(Integer.toString(j));
					anyWithin = true;
					within++;
					break;
				}
			}
			// どの行の範囲にも該当していない場合
			if(!anyWithin){
				lineNumber.add("Out of Range");
			}
		}
		data.getColumnSplitData().set(data.getXIndex(), lineNumber); // 視線のX座標の列を(X,Y)座標に対応する行番号に置き換える
		data.getColumnSplitData().remove(pointY);
		outputData = data.convertToLines(); // 視線座標データをカンマ区切りのファイルに書き込みやすいデータに変換
		outputData.add(0, "Within Rate = " + (double)within / total);
	}
	
	/**
	 * {@link #toNodeTransition(List)}から取得したList構造のデータから特定の状態を持つデータを取り除きます。
	 * 
	 * @param pattern 除外する行に含まれる特定の文字列
	 */
	public void ignoreParticularData(String pattern){
		List<String> stub = new ArrayList<String>();
		Pattern p = Pattern.compile(pattern);
		for(String line : outputData){
			if(p.matcher(line).find()){
				stub.add(line);
			}
		}
		outputData.removeAll(stub);
	}
}
