package generate.code.io;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * データベース接続・切断用ユーティリティクラスです。<br>
 * データベースを使った処理では必ずメソッド単位でこのクラスの{@link #connect()}と{@link #disconnect()}を呼び出しましょう。<br>
 * データベースに接続した後Statement作成などのために必要なConnectionインスタンスは{@link #getConnection()}で取得します。
 * @author 花房亮
 *
 */
public class DatabaseConnection {

	private final static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private final static String USER_NAME = "generator";
	private final static String PASSWORD = "generator";
	private final static String DB_URL = "jdbc:mysql://localhost/sourceCodeGenerator";
	private static Connection conn = null;
	
	/**
	 * データベースに接続します。
	 */
	public static void connect(){
		// JDBCドライバを読み込む
		try {
			Class.forName( JDBC_DRIVER ).newInstance();
			// 接続
			Properties prop = new Properties();
			prop.put( "user",  USER_NAME);
			prop.put( "password", PASSWORD );
			prop.put( "characterEncoding", "utf8" );
			conn = DriverManager.getConnection( DB_URL, prop );
		}catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e){
			e.printStackTrace();
			if( conn != null ){
				try {
					conn.rollback();
					conn.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * データベースから切断します。
	 */
	public static void disconnect(){
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			if( conn != null ){
				try {
					conn.rollback();
					conn.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * {@link java.sql.Connection}を返します。
	 * 
	 * @return {@link java.sql.Connection}
	 */
	public static Connection getConnection(){
		return conn;
	}
}
