package generate.code.util;

/**
 * ファイル名を操作するためのユーティリティクラスです。
 * @author 花房亮
 *
 */
public class FileNameUtility {
	
	/**
	 * 拡張子を含むファイル名から拡張子を含まないファイル名を返します
	 * @param fileName 拡張子を含むファイル名
	 * @return 拡張子を含まないファイル名
	 */
	public static String getNameWithoutExtension(String fileName){
		int point = fileName.lastIndexOf(".");
		return fileName.substring(0, point);
	}
}
