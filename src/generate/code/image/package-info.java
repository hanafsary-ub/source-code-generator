/**
 * スクリーンショット画像からコードの範囲を決定するクラスを提供します。
 * @author 花房亮
 *
 */
package generate.code.image;