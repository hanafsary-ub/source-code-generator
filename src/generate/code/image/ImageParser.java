package generate.code.image;

import java.awt.AWTException;
import java.awt.Desktop;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.imageio.ImageIO;

import generate.code.graph.dependence.data.Coordinate;
import generate.code.graph.dependence.data.DataDependenceGraph;
import generate.code.graph.dependence.data.LineRange;
import generate.code.graph.dependence.data.Node;
import generate.code.io.EyeTrackingData;

/**
 * HTML出力したソースコードの領域をスクリーンショット画像から特定するクラスです。<br>
 * {@link #parse(int)}を呼び出す前に{@link #takeScreenShot(String)}でHTMLファイルのスクリーンショット画像を撮ってください。<br>
 * {@link #openHTML(String)}で自動的にHTMLファイルを開くこともできます。
 * ただし、{@link #openHTML(String)}を呼び出してから、ソースコードがブラウザで表示されるまでは環境によって異なる時間がかかります。<br>
 * よって、{@link #openHTML(String)}を呼び出してから{@link #takeScreenShot(String)}を呼び出す前に数秒時間を置くようにしてください。
 * 
 * @author 花房亮
 *
 */
public class ImageParser {

	private DataDependenceGraph graph;     // ノードの領域を設定する用
	private String inputPath;              // 出力HTMLファイルのあるディレクトリパス
	private String outputPath;             // スクリーンショット画像を保存するディレクトリパス
	private BufferedImage image;           // 撮影されたスクリーンショット画像
	private int margin;                    // 出力されたソースコードの一行ごとの余白(行間の間隔から求める)
	private final int[] rangeColor = {255,0,0}; // 範囲を画像出力する際の色
	private final int[] pointColor = {0,255,0}; // 視線座標を画像上に反映させる際の色

	//TODO:Javadoc追記する
	public ImageParser(){
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("./data/settings/data_path.properties")); // プロパティファイルの読み込み
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.inputPath = prop.getProperty("path.output.html") + "/";
		this.outputPath = prop.getProperty("path.output.image") + "/";
		try {
			prop.load(new FileInputStream("./data/settings/html_format.properties")); // プロパティファイルの読み込み
		} catch (IOException e) {
			e.printStackTrace();
		} 
		this.margin = Integer.parseInt(prop.getProperty("format.line.height")) / 2;// XXpxのXXの部分の数字を取り出し、その半分の値を余白とする
	}

	/**
	 * ソースコードの情報を保持したデータ依存グラフを指定してImageParserを構築します。<br>
	 * HTMLファイルが保存されているディレクトリのパスと出力画像を保存するディレクトリのパスはdata_path.propertiesファイルで設定します。<br>
	 * また、HTMLファイルを出力する際に決めた行間情報をhtml_format.propertiesファイルから取得し利用します。<br>
	 * 引数のデータ依存グラフはノードごとの領域の情報を追加するために使用します。
	 * {@link #parse(int)}で領域を特定してデータ依存グラフを返します。
	 * 
	 * @param graph 生成ソースコードの情報を対応付けたデータ依存グラフ
	 */
	public ImageParser(DataDependenceGraph graph){
		this();
		this.graph = graph;
	}

	/**
	 * ファイル名を指定して、ソースコードのHTMLファイルを開きます。<br>
	 * ファイル名は拡張子を除いたものを指定します。
	 * 
	 * @param fileName 拡張子を除いたファイル名
	 */
	public void openHTML(String fileName){
		Desktop desktop = Desktop.getDesktop();
		try {
			// 規定のブラウザでHTMLファイルを開く
			desktop.open(new File(this.inputPath + fileName + ".html"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 画面のスクリーンショットを撮り、保存します。<br>
	 * スクリーンショットが撮れたタイミングでビープ音を鳴らします。
	 * {@link #openHTML(String)}でブラウザを開く場合はブラウザがソースコードを表示するのに十分な時間待ってください。<br>
	 * ファイル名は拡張子を除いたものを指定します。
	 * 
	 * @param fileName 拡張子を除いたファイル名
	 */
	public void takeScreenShot(String fileName){
		 try {
			//スクリーンショットを撮る
			this.image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
			//スクリーンショットが撮れたらビープ音を鳴らす
			Toolkit.getDefaultToolkit().beep();
			//撮った画像を保存する
			ImageIO.write(this.image, "png", new File(this.outputPath + fileName + ".png"));
		} catch (HeadlessException | AWTException | IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 画像に各行の範囲を表示した画像をファイルに書き込みます。<br>
	 * ファイル名は拡張子を除いたものを指定します。
	 * 
	 * @param fileName 拡張子を除いたファイル名
	 */
	public void writeRange(String fileName){
		for(Node node : this.graph.getNodes()){
			LineRange range = node.getRange();
			int top = range.getTopLeft().getY();
			int bottom = range.getBottomLeft().getY();
			int left = range.getTopLeft().getX();
			int right = range.getTopRight().getX();
			int r = this.rangeColor[0], g = this.rangeColor[1], b = this.rangeColor[2];
			for(int x = left; x <= right; x++){
				this.image.setRGB(x, top, ImageUtility.getRGB(r, g, b));
				this.image.setRGB(x, bottom, ImageUtility.getRGB(r, g, b));
			}
			for(int y = top; y <= bottom; y++){
				this.image.setRGB(left, y, ImageUtility.getRGB(r, g, b));
				this.image.setRGB(right, y, ImageUtility.getRGB(r, g, b));
			}
		}
		try {
			ImageIO.write(this.image, "png", new File(this.outputPath + fileName + "(範囲表示).png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 範囲表示した問題文の画像に視線の座標データを描画します。<br>
	 * 
	 * @param inputFileName もとの範囲表示画像と座標データを上書きした出力画像のファイル名。拡張子は含まれない。
	 * @param outputFileName 視線座標をプロットした出力画像のファイル名。拡張子は含まれない。
	 * @param data 画面上の視線推移データ
	 */
	public void writeTrackingPoint(String inputFileName, String outputFileName, EyeTrackingData data){
		try {
			this.image = ImageIO.read(new File(this.outputPath + inputFileName + "(範囲表示).png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		int width = this.image.getWidth(), height = this.image.getHeight();

		for(String[] line : data.getContent()){
			
			if(line[data.getXIndex()].equals("null") || line[data.getYIndex()].equals("null")){
				continue;
			}
			
			int x = Integer.parseInt(line[data.getXIndex()]);
			int y = Integer.parseInt(line[data.getYIndex()]);
			int r = pointColor[0], g = pointColor[1], b = pointColor[2];
			// 画面領域内なら
			if(x >= 1 && x < width - 1 && y >= 1 && y < height - 1){
				this.image.setRGB(x    , y    , ImageUtility.getRGB(r, g, b));
				this.image.setRGB(x - 1, y    , ImageUtility.getRGB(r, g, b));
				this.image.setRGB(x + 1, y    , ImageUtility.getRGB(r, g, b));
				this.image.setRGB(x    , y - 1, ImageUtility.getRGB(r, g, b));
				this.image.setRGB(x    , y + 1, ImageUtility.getRGB(r, g, b));
			}
		}
		try {
			ImageIO.write(this.image, "png", new File(this.outputPath + "plot/" + outputFileName + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * スクリーンショットの画像からコードの範囲を特定し、それをノードに対応付けたデータ依存グラフを返します。<br>
	 * コードの範囲を特定する前に、(length)x(スクリーンの幅)の大きさの探索矩形でコードの書いてあるブラウザのページ表示部の上下の範囲を特定し、<br>
	 * (スクリーンの高さ)x(length)の大きさの探索矩形でページ表示部の左右の範囲を特定します。<br>
	 * 基本的にはlengthの値は大きければ大きいほど、正確にページ領域を求めることができ、小さければ小さいほどページ領域を大きく取ることができます。<br>
	 * 望ましいlengthの大きさは3px以上、html_format.propertiesで設定したmarginの半分までが望ましいです。
	 * 
	 * @param length コードの書いてあるページ表示部の範囲を探索するための矩形の長さ
	 * @return コードの範囲を対応付けたデータ依存グラフ
	 */
	public DataDependenceGraph parse(int length){
		int height = this.image.getHeight();
		int width = this.image.getWidth();
		int startY = -1, endY = -1;
		double whiteRate = 0.9;
		/*ブラウザのツールバーなどの領域を無視するため、探索区間のページ表示部を求める*/
		for(int y = 0; y < height; y += length){
			int whiteCount = 0;
			for(int yi = y; yi <  y + length && yi < height; yi++){
				for(int x = 0; x < width; x++){
					int r = ImageUtility.getRed(image.getRGB(x, yi));
					int g = ImageUtility.getGreen(image.getRGB(x, yi));
					int b = ImageUtility.getBlue(image.getRGB(x, yi));
					if(r == 255 && g == 255 && b == 255){
						whiteCount++;
					}
				}
			}
			double rate = (double)whiteCount / (double)(length * width);
			if( rate > whiteRate){ // 白の割合がwhiteRate以上のピクセル行のうち、一番最初に見つけたものを開始点、最後に見つけたものを終了点とする
				if(startY == -1){
					startY = y + length;
				}else{
					endY = y;
				}
			}
		}
		int startX = -1, endX = -1;
		for(int x = 0; x < width; x += length){
			int whiteCount = 0;
			for(int xi = x; xi < x + length && xi < width; xi++){
				for(int y = startY; y < endY; y++){
					int r = ImageUtility.getRed(image.getRGB(xi, y));
					int g = ImageUtility.getGreen(image.getRGB(xi, y));
					int b = ImageUtility.getBlue(image.getRGB(xi, y));
					if(r == 255 && g == 255 && b == 255){
						whiteCount++;
					}
				}
			}
			double rate = (double)whiteCount / (double)(length * (endY - startY));
			if( rate > whiteRate){ // 白の割合がwhiteRate以上のピクセル"列"のうち、一番最初に見つけたものを開始点、最後に見つけたものをを終了点とする
				if(startX == -1){
					startX = x + length;
				}else{
					endX = x;
				}
			}
		}
		//System.out.println("startX=" + startX + ",endX=" + endX + ",startY=" + startY + ",endY=" + endY);
		/*1行目のコードを探す*/
		int top = startY, bottom = endY, left = startX, right = endX;
		/*1行目上部の探索*/
		top = this.findTop(startX, endX, startY, endY);
		/*1行目下部の探索*/
		bottom = this.findBottom(left, endX, top, endY);
		/*1行目左部の探索*/
		left = this.findLeft(startX, endX, top, bottom);
		/*1行目右部の探索*/
		right = this.findRight(endX, left, top, bottom);
		// 余白を考慮して1行目の範囲を求める
		top -= this.margin;
		left -= this.margin;
		bottom += this.margin;
		right += this.margin;
		Coordinate leftTop = new Coordinate(left, top);
		Coordinate rightTop = new Coordinate(right, top);
		Coordinate leftBottom = new Coordinate(left, bottom);
		Coordinate rightBottom = new Coordinate(right, bottom);
		this.graph.getNodes().get(0).setRange(new LineRange(leftTop, rightTop, leftBottom, rightBottom));
		for(int i = 1; i < this.graph.getNodes().size(); i++){
			int tempTop = bottom; // 1行目下部を2行目上部にする。つまり、i-1行目下部をi行目上部にする。
			//　左部はそのまま(インデント有りなら探索する必要あり)で、下部と右部を決める。
			// 下部を決めるために余白なしの上部を求める
			top = this.findTop(startX, endX, tempTop, endY);
			// 下部を求める
			bottom = this.findBottom(left, endX, top, endY);
			// 右部を求める
			right = this.findRight(endX, left, top, bottom);

			top = tempTop + 1;
			bottom += this.margin;
			right += this.margin;
			leftTop = new Coordinate(left, top);
			rightTop = new Coordinate(right, top);
			leftBottom = new Coordinate(left, bottom);
			rightBottom = new Coordinate(right, bottom);
			this.graph.getNodes().get(i).setRange(new LineRange(leftTop, rightTop, leftBottom, rightBottom));
		}
		return this.graph;
	}

	// プログラムの行の中で文字が書いてある一番上のピクセル行を探す。
	private int findTop(int startX, int endX, int startY, int endY){
		int top = -1;
		TOP:
		for(int y = startY; y < endY; y++){
			for(int x = startX; x < endX; x++){
				int r = ImageUtility.getRed(image.getRGB(x, y));
				int g = ImageUtility.getGreen(image.getRGB(x, y));
				int b = ImageUtility.getBlue(image.getRGB(x, y));
				if(r != 255 || g != 255 || b != 255){//白でなかったらその行の上部確定
					top = y;
					break TOP;
				}
			}
		}
		return top;
	}
	
	// プログラムの行の中で文字が書いてある一番下のピクセル行を探す。
	private int findBottom(int startX, int endX, int startY, int endY){
		int bottom = -1;
		for(int y = startY; y < endY; y++){
			boolean allWhite = true;
			for(int x = startX; x < endX; x++){
				int r = ImageUtility.getRed(image.getRGB(x, y));
				int g = ImageUtility.getGreen(image.getRGB(x, y));
				int b = ImageUtility.getBlue(image.getRGB(x, y));
				if(r != 255 || g != 255 || b != 255){
					allWhite = false;
					break;
				}
			}
			if(allWhite){//区間内ですべて白のピクセル行があればその1つ前がその行の下部
				bottom = y - 1;
				break;
			}
		}
		return bottom;
	}
	
	// 左から1列ピクセルずつ走査して文字が書いてある一番左側のx座標を求める
	private int findLeft(int startX, int endX, int startY, int endY){
		int left = -1;
		LEFT:
		for(int x = startX; x < endX; x++){
			for(int y = startY; y < endY; y++){
				int r = ImageUtility.getRed(image.getRGB(x, y));
				int g = ImageUtility.getGreen(image.getRGB(x, y));
				int b = ImageUtility.getBlue(image.getRGB(x, y));
				if(r != 255 || g != 255 || b != 255){//白でなかったら1行目の左部確定
					left = x;
					break LEFT;
				}
			}
		}
		return left;
	}
	
	// 右から1列ピクセルずつ走査して文字が書いてある一番右側のx座標を求める。
	private int findRight(int startX, int endX, int startY, int endY){
		int right = -1;
			RIGHT:
			for(int x = startX; x > endX; x--){
				for(int y = startY; y < endY; y++){
					int r = ImageUtility.getRed(image.getRGB(x, y));
					int g = ImageUtility.getGreen(image.getRGB(x, y));
					int b = ImageUtility.getBlue(image.getRGB(x, y));
					if(r != 255 || g != 255 || b != 255){
						right = x;
						break RIGHT;
					}
				}
			}
		return right;
	}
}
