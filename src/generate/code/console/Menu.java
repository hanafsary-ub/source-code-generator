package generate.code.console;

import java.io.File;

import generate.code.console.AbstractMenu.Content;

/**
 * コマンドライン形式のメニューを表現するためのインターフェースです。<br>
 * {@link #start()}でメニューを立ち上げて操作します。<br>
 * 
 * @author hanafusaryo
 *
 */
public interface Menu {

	/**
	 * 番号を入力して選択する形式のメニューを立ち上げます。<br>
	 * メニューは標準出力で表示され、標準入力で操作します。
	 */
	void start();
	
	/**
	 * 選択可能なファイルのリストを列挙したメニューの内容を取得します。
	 * 
	 * @param files ファイルのリスト
	 * @param instruction 指示内容
	 * @param extensionType 取得するファイルの拡張子
	 * @return　メニューの内容
	 */
	Content retrieveContent(File[] files, String instruction, String extensionType);
	
}
