package generate.code.console;

import static generate.code.io.EyeTrackingData.EYETRIBE_DAT;
import static generate.code.io.EyeTrackingData.TOBII_TSV;
import generate.code.graph.dependence.data.LineRange;
import generate.code.image.ImageParser;
import generate.code.io.EyeTrackingAnalyzer;
import generate.code.io.RelationStorage;
import generate.code.util.FileNameUtility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;
import java.util.Properties;

/**
 * 視線データを入力としてノード間の推移で表したデータを出力したい場合に実行するクラスです。<br>
 * 視線データに対応する問題をHTMLファイルから選び、対応する視線データを選びます。
 * 
 * @author 花房亮
 *
 */
public class EyeTrackingAnalysisMenu extends AbstractMenu{

	private String inputHTMLPath;       // 生成ソースコードを出力したHTMLファイルが入っているディレクトリのパス
	private String inputTrackingPath;   // 視線データの入っているディレクトリのパス
	private String outputPath;          // ノードの推移で表した視線データを出力するディレクトリのパス
	private int dataType;               // 読み込む視線データのデータ形式
	private boolean nullData = false;   // ノードの推移を表す際に座標を取得できなかったデータを含めるかどうか
	private boolean outOfRange = false; // ノードの推移を表す際に範囲外の視線データを含めるかどうか

	/**
	 * Propertiesファイルを読み込み、EyeTrackingAnalysisMenuを構築します。
	 * 
	 */
	public EyeTrackingAnalysisMenu(){
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("./data/settings/data_path.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.inputTrackingPath = prop.getProperty("path.input.eyetracking");
		this.inputHTMLPath = prop.getProperty("path.output.html");
		this.outputPath = prop.getProperty("path.output.transition");
		try {
			prop.load(new FileInputStream("./data/settings/node_transition.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(prop.getProperty("ignore.null").equals("true")) this.nullData = true;
		if(prop.getProperty("ignore.outofrange").equals("true")) this.outOfRange = true;
		try {
			prop.load(new FileInputStream("./data/settings/device.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.dataType = Integer.parseInt(prop.getProperty("device.number"));
	}
	
	/**
	 * 番号を入力して選択する形式のメニューを立ち上げます。<br>
	 * 最初に視線データに対応するソースコードのHTMLファイルを選択し、ノードの範囲をデータベースから読み込みます。<br>
	 * 次に対応する視線データを選び、その視線データをノードの推移で表したものに変換し、ファイルに書き込みます。
	 * 
	 */
	public void start(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		File[] htmlFiles = new File(this.inputHTMLPath).listFiles();
		File[] eyeTrackingFiles = new File(this.inputTrackingPath).listFiles();
		int selection1 = -1, selection2 = -1;

		while(true){
			
			/* HTMLファイルの列挙 */
			Content htmlMenu = this.retrieveContent(htmlFiles, "問題文のHTMLファイルを選んでください。", "html");
			htmlMenu.getMenu().forEach(s -> System.out.println(s));

			/* キーボードから番号の入力 */
			try {
				selection1 = Integer.parseInt(br.readLine());
			} catch (NumberFormatException | IOException e) {
				e.printStackTrace();
				break;
			}

			if(!htmlMenu.getSelectableIDs().contains(selection1)) break;//選択できる番号でなかったら終了する

			RelationStorage storage = new RelationStorage();
			storage.findID(htmlFiles[selection1]);
			List<LineRange> ranges = storage.loadRange();
			System.out.println("対応するコードの範囲を読み込みました。");

			/* 視線データの列挙 */
			String extension = ""; // 拡張子
			if(this.dataType == TOBII_TSV){ // Tobiiアイトラッカーで出力したTSVファイルなら
				extension = "tsv";
			}else if(this.dataType == EYETRIBE_DAT){ // TheEyeTribeで出力したDATファイルなら
				extension = "dat";
			}
			Content eyeTracking = this.retrieveContent(eyeTrackingFiles, "分析対象の視線データファイルを番号で選んでください。", extension);
			eyeTracking.getMenu().forEach(s -> System.out.println(s));

			/* キーボードから番号の入力 */
			try {
				selection2 = Integer.parseInt(br.readLine());
			} catch (NumberFormatException | IOException e) {
				e.printStackTrace();
				break;
			}

			if(!eyeTracking.getSelectableIDs().contains(selection2)) break;//選択できる番号でなかったら終了する

			File file = eyeTrackingFiles[selection2];
			// 視線データファイルの拡張子を除いたファイル名を取得
			String trackingName = FileNameUtility.getNameWithoutExtension(file.getName());
			EyeTrackingAnalyzer analyzer = new EyeTrackingAnalyzer();
			try {
				PrintWriter pw = new PrintWriter(this.outputPath + "/" + trackingName + ".csv", "UTF-8");
				boolean readable = false;
				if(this.dataType == TOBII_TSV){
					readable = analyzer.readTSV(file);// TSVファイルを読み込む
				}else if(this.dataType == EYETRIBE_DAT){
					readable = analyzer.readDAT(file);// DATファイルを読み込む
				}
			    if(readable){                                  
			    	analyzer.toNodeTransition(ranges);                               // ノードの推移に変換する
			    	if(this.nullData){
			    		analyzer.ignoreParticularData("null");                       // nullを無視する設定なら無視する
			    	}
			    	if(this.outOfRange){
			    		analyzer.ignoreParticularData("Out of Range");               // 範囲外の状態を無視する設定なら無視する
			    	}
			    	List<String> output = analyzer.getOutputData(); 
			    	output.add(0, "SourceCodeID = " + storage.getGeneratedCodeID()); // 先頭行にソースコードに対応づけるためのIDを追加する
			    	output.forEach(s -> pw.println(s));                              // ファイルに書き込む
			    }
			    pw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			String htmlName = FileNameUtility.getNameWithoutExtension(htmlFiles[selection1].getName()); // 拡張子なしのファイル名
			
			// 推移座標を画像出力する
			ImageParser parser = new ImageParser();
			parser.writeTrackingPoint(htmlName, trackingName, analyzer.getEyeTrackingData());
			System.out.println("視線データの画像を出力しました。");
		}
		
	}

	public static void main(String[] args){
		new EyeTrackingAnalysisMenu().start();
	}

}
