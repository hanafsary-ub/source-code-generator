package generate.code.console;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Menuの基底実装クラスです。<br>
 * {@link #retrieveContent(File[], String, String)}を実装しています。
 * 
 * @author 花房亮
 *
 */
public abstract class AbstractMenu implements Menu{
	
	/**
	 * 番号を入力して選択する形式のメニューを立ち上げます。<br>
	 * メニューは標準出力で表示され、標準入力で操作します。
	 */
	@Override
	public abstract void start();
	

	/**
	 * 選択可能なファイルのリストを列挙したメニューの内容を取得します。
	 * 
	 * @param files ファイルのリスト
	 * @param instruction 指示内容
	 * @param extensionType 取得するファイルの拡張子
	 * @return　メニューの内容
	 */
	@Override
	public Content retrieveContent(File[] files, String instruction, String extensionType){
		Content content = new Content();
		content.getMenu().add(instruction);
		content.getMenu().add("-1: 終了する");
		for(int i = 0; i < files.length; i++){
			String fullName = files[i].getName();
			int begin = fullName.lastIndexOf(".") + 1;
			String extension = fullName.substring(begin, fullName.length());
			if(extension.equals(extensionType)){
				content.getSelectableIDs().add(i);
				content.getMenu().add(i + ": " + fullName);
			}
		}
		return content;
	}
	/**
	 * メニュー内容と選択可能なファイルの番号のリストを持つ内部クラス。
	 * @author 花房亮
	 *
	 */
	public class Content{
		private List<String> menu = new ArrayList<>();
		private List<Integer> selectableIDs = new ArrayList<>();
		
		/**
		 * メニューの内容を返します。
		 * @return メニューの内容
		 */
		public List<String> getMenu(){
			return this.menu;
		}
		
		/**
		 * 選択可能なファイルの番号を返します。<br>
		 * 
		 * @return 選択可能なファイルの番号
		 */
		public List<Integer> getSelectableIDs(){
			return this.selectableIDs;
		}
	}
}
