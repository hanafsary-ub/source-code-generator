package generate.code.console;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import generate.code.graph.dependence.data.DataDependenceGraph;
import generate.code.image.ImageParser;
import generate.code.io.RelationStorage;
import generate.code.util.FileNameUtility;

public class ScreenShotRetakeMenu extends AbstractMenu{
	
	private String htmlPath;
	
	public ScreenShotRetakeMenu(){
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("./data/settings/data_path.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.htmlPath = prop.getProperty("path.output.html");
	}
	@Override
	public void start() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		File[] htmlFiles = new File(this.htmlPath).listFiles();
		int inputIndex = -1;
		
		while(true){
			Content htmlMenu = this.retrieveContent(htmlFiles, "スクリーンショットを撮り直したい問題文のHTMLファイルを選んでください。", "html");
			htmlMenu.getMenu().forEach(s -> System.out.println(s));
			
			try {
				inputIndex = Integer.parseInt(br.readLine());
			} catch (NumberFormatException | IOException e) {
				e.printStackTrace();
			}
			if(!htmlMenu.getSelectableIDs().contains(inputIndex)) break;//選択できる番号でなかったら終了する
			
			RelationStorage storage = new RelationStorage();
			storage.findID(htmlFiles[inputIndex]);
			
			String fileName = FileNameUtility.getNameWithoutExtension(htmlFiles[inputIndex].getName());
			DataDependenceGraph ddg = new DataDependenceGraph(storage.fetchSize());

			ImageParser parser = new ImageParser(ddg);
			System.out.println("HTMLファイルを規定のブラウザで開いたあと、スクリーンショットを撮ります。");
			System.out.println("ソースコードが表示されるまでの時間を考慮して、撮影までの時間を指定してください(秒)");
			try{
				long wait = Long.parseLong(br.readLine()) * 1000L; // 時間の入力
				parser.openHTML(fileName);                         // HTMLファイルを規定のブラウザで開く
				Thread.sleep(wait);
				parser.takeScreenShot(fileName);                   // スクリーンショットをとる
				System.out.println("スクリーンショットの撮影が完了しました。");
				parser.parse(8);                                   // コードの範囲を求める
				parser.writeRange(fileName);                       // コードの範囲を画像で出力する
				System.out.println("コードの範囲を特定しました。");
				storage.updateLineRange(ddg);                      // 新たに取得した行範囲でデータベースを更新する
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
	}

	public static void main(String[] args){
		new ScreenShotRetakeMenu().start();
	}
}
