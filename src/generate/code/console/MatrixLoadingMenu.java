package generate.code.console;

import generate.code.graph.dependence.data.DataDependenceGraph;
import generate.code.image.ImageParser;
import generate.code.io.MatrixLoader;
import generate.code.io.RelationStorage;
import generate.code.io.SourceCodeLayout;
import generate.code.io.exception.DataDependenceException;
import generate.code.util.FileNameUtility;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;

/**
 * 隣接行列からソースコードを生成したい場合に実行するクラスです。<br>
 * ファイルの指定はファイル名ではなく、ファイルに割り当てられたIDを入力して指定します。
 * 
 * @author 花房亮
 *
 */
public class MatrixLoadingMenu extends AbstractMenu{

	private String path; // 隣接行列の書かれたファイルを読み込むディレクトリ
	
	public MatrixLoadingMenu(){
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("./data/settings/data_path.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.path = prop.getProperty("path.input.matrix");
	}

	/**
	 * 入力隣接行列を入力用のフォルダから選びコードの自動生成を行うメニューを起動します。
	 * 
	 */
	@Override
	public void start(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		File dir = new File(path);
		File[] files = dir.listFiles();

		int id = -1;
		while(true){
			Content menu = this.retrieveContent(files, "ソースコードを生成したい隣接行列のファイル番号を入力してください。", "csv");
			menu.getMenu().forEach(s -> System.out.println(s));

			// 番号の入力
			try {
				id = Integer.parseInt(br.readLine());
			} catch (NumberFormatException | IOException e) {
				e.printStackTrace();
				break;
			}

			if(!menu.getSelectableIDs().contains(id)) break;//選択できる番号でなかったら終了する
				
			try {
				// 隣接行列の読み込み
				List<String[]> matrix = MatrixLoader.checkDataDependenceException(MatrixLoader.readCSV(files[id]));
				// データ依存グラフの生成
				DataDependenceGraph ddg = new DataDependenceGraph(matrix);
				SourceCodeLayout scl;
				do{
					ddg.generateNodes();
					//ソースコードの構築
					scl = new SourceCodeLayout(ddg);
					scl.buildCode();
					String[] sample = scl.getCode();//生成したソースコードを取得する
					//ソースコードをサンプルとして出力する
					System.out.println("------------------------");
					for(String line : sample){
						System.out.println(line);
					}
					System.out.println("------------------------");

					System.out.println("以上が生成されたソースコードです。再度生成しますか?(y/n)");
				}while(br.readLine().equals("y"));

				// 拡張子を除いたファイル名を取得
				String fileName = FileNameUtility.getNameWithoutExtension(files[id].getName());
				ImageParser parser = new ImageParser(ddg);
				/* htmlファイルを出力しブラウザで開く。書式を確認させ、良ければもう一度開きスクリーンショットを撮る
				 * 納得の行くものでなければhtml_format.propertiesを変更させ、(propertiesファイルの関連付けは？)もう一度htmlファイルを出力する。
				 */
				do{
					scl.printToHTML(fileName); // HTMLファイルに出力
					parser.openHTML(fileName);
					System.out.println("ソースコードのHTMLファイルを生成しました。書式を変更してHTMLファイルの再生成を行いますか。(y/n)");
					if(br.readLine().equals("y")){
						System.out.println("html_format.propertiesを既定のプログラムで開きます。設定を変更してください。(完了したらEnterキーを押してください)");
						Desktop desktop = Desktop.getDesktop();
						// html_format.propertiesを既定のプログラムで開く
						desktop.open(new File("./data/settings/html_format.properties"));
						br.readLine();
						// プロパティファイルを再読み込み
						scl.reloadHTMLFormat();
					}else{
						break;
					}
				}while(true);

				/* 画面上のコードの範囲の特定 */
				System.out.println("HTMLファイルを規定のブラウザで開いたあと、スクリーンショットを撮ります。");
				System.out.println("ソースコードが表示されるまでの時間を考慮して、撮影までの時間を指定してください(秒)");
				long wait = Long.parseLong(br.readLine()) * 1000L; // 時間の入力
				parser.openHTML(fileName);                         // HTMLファイルを規定のブラウザで開く
				Thread.sleep(wait);
				parser.takeScreenShot(fileName);                   // スクリーンショットをとる
				System.out.println("スクリーンショットの撮影が完了しました。");
				parser.parse(8);                                   // コードの範囲を求める
				parser.writeRange(fileName);                       // コードの範囲を画像で出力する
				System.out.println("コードの範囲を特定しました。");
				//ノード情報をデータベースに保存する
				new RelationStorage().save(ddg, scl);

			} catch (DataDependenceException | IOException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		new MatrixLoadingMenu().start();
	}
}
