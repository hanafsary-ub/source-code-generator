# rootでログインして接続用ユーザとデータベースとテーブルを作成します

# データベース(スキーマ)の作成
CREATE DATABASE sourceCodeGenerator;

# ユーザの作成と権限の付与
GRANT ALL PRIVILEGES ON sourceCodeGenerator.* TO generator@localhost IDENTIFIED BY 'generator' WITH GRANT OPTION;
FLUSH PRIVILEGES;

# データベース(スキーマ)の選択
USE sourceCodeGenerator;

# テーブルの作成
CREATE TABLE SourceCode(
    generatedCodeID INT  NOT NULL, # HTML出力されたソースコードのID
    adjacencyMatrix TEXT NOT NULL, # カンマ区切りの隣接行列
    matrixSize      INT  NOT NULL, # 隣接行列の大きさ
    PRIMARY KEY(generatedCodeID)
);

CREATE TABLE LineRange(
    generatedCodeID INT NOT NULL,
    lineRangeID INT NOT NULL, # ノードの矩形領域のID
    leftTopX    INT NOT NULL, # 矩形の左上のx座標
    leftTopY    INT NOT NULL, # 矩形の左上のy座標
    width       INT NOT NULL, # 矩形の幅
    height      INT NOT NULL, # 矩形の高さ
    FOREIGN KEY(generatedCodeID) REFERENCES SourceCode(generatedCodeID)
);

CREATE TABLE Node(
    generatedCodeID INT          NOT NULL, # ノードの所属するソースコードのID
    nodeID          INT          NOT NULL, # ノードに対応するID
    programSentence VARCHAR(256) NOT NULL, # ノードの対応するソースコードの行の内容
    FOREIGN KEY(generatedCodeID) REFERENCES SourceCode(generatedCodeID)
);

